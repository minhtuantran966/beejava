package com.chat.bee.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Minh Tuấn
 * 8:42 CH 13/03/2021/03/2021
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReactionDTO {

    private String type;
    private UserDTO user;
    private int count;

}
