package com.chat.bee.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private String id;
    private String firstName;
    private String lastName;
    private String gender;
    private Date birthday;
    private String countryCode;
    private String phone;
    private String avatarUrl;
    private boolean online;
    private Date lastActive;
    private boolean friend;
    private boolean requested;
    private boolean receiveRequest;
    private String groupId;

}
