package com.chat.bee.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Minh Tuấn
 * 14:51 04/06/2021
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatNotificationDTO {

    private UserDTO from;
    private UserDTO to;
    private String type;

}