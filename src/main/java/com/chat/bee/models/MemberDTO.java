package com.chat.bee.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberDTO {

    private UserDTO user;
    private String role;
    private Date lastedSeen;
    private String nickName;

}
