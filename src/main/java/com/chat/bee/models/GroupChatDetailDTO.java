package com.chat.bee.models;

import lombok.Data;

import java.util.List;

/**
 * Minh Tuấn
 * 8:35 CH 13/03/2021/03/2021
 */

@Data
public class GroupChatDetailDTO {

    private GroupChatDTO groupChat;
    private List<ChatDTO> chats;
    private List<ChatDTO> pinChats;

}
