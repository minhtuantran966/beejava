package com.chat.bee.models;

import com.chat.bee.entities.ChatStyle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupChatDTO {

    private String id;
    private String name;
    private String type;
    private String createBy;
    private Date createTime;
    private String avatar;
    private ChatDTO lastedChat;
    private List<MemberDTO> members;
    private ChatStyleDTO chatStyle;

}
