package com.chat.bee.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Minh Tuấn
 * 8:45 CH 13/03/2021/03/2021
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttachmentDTO {

    private String id;
    private String chatId;
    private String groupId;
    private String type;
    private String link;
    private String title;
    private String iconLink;
    private String description;
    private String imageLink;
    private int duration;

}
