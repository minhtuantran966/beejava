package com.chat.bee.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Minh Tuấn
 * 15:33 04/06/2021
 */

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatStyleDTO {

    private String color;
    private String background;
    private String emoji;

}