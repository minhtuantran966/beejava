package com.chat.bee.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatDTO {

    private String id;
    private String groupId;
    private String content;
    private String type;
    private ChatNotificationDTO notification;
    private List<AttachmentDTO> attachments;
    private List<ReactionDTO> reactions;
    private List<UserDTO> mentionUsers;
    private ChatDTO parentChat;
    private UserDTO chatBy;
    private Date chatTime;
    private boolean isSystem;
    private boolean isPin;

}
