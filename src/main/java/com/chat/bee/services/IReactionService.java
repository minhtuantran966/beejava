package com.chat.bee.services;

import com.chat.bee.entities.Reaction;
import com.chat.bee.models.ReactionDTO;
import com.chat.bee.models.UserDTO;

import java.util.List;
import java.util.Map;

public interface IReactionService {

    List<Reaction> getReactionByChatIds(List<String> chatIds);

    Map<String, List<ReactionDTO>> mapChatIdToReactions(List<Reaction> reactions, Map<String, UserDTO> mapUser);

}
