package com.chat.bee.services;

import com.chat.bee.entities.UserLogin;
import com.chat.bee.models.GroupChatDTO;
import com.chat.bee.models.UserDTO;

import java.util.List;
import java.util.Map;

public interface IUserService {

    boolean phoneIsExits(String countryCode, String phone);

    Map<String, UserDTO> getMapUser(List<String> userIds);

    UserDTO createChatUser(UserDTO user, String password);

    UserLogin getUserLogin(String username);

    UserDTO getUserById(String userId);

    List<UserDTO> searchUser(String query);

    void addRequest(String friendId, String groupId);

    void removeRequest(String friendId, String groupId);

    String acceptRequest(String friendId, String groupId);

    void rejectRequest(String friendId, String groupId);

    void resetPassword(String countryCode, String phone, String password);

    boolean changePassword(String oldPassword, String newPassword);

    void updateMyInfo(UserDTO user);

    List<UserDTO> friendList();

    List<UserDTO> requestContactList();

    List<UserDTO> getUserByPhone(List<String> phones);

}
