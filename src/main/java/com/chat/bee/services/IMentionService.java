package com.chat.bee.services;

import com.chat.bee.entities.Mention;
import com.chat.bee.models.UserDTO;

import java.util.List;
import java.util.Map;

/**
 * Minh Tuấn
 * 11:26 CH 13/03/2021/03/2021
 */

public interface IMentionService {

    void save(Mention mention);

    List<Mention> getMentionsByChatIds(List<String> chatIds);

    Map<String, List<UserDTO>> mapChatIdToMentionUsers(List<Mention> mentions, Map<String, UserDTO> userMap);

}
