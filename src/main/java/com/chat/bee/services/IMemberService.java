package com.chat.bee.services;

import com.chat.bee.models.MemberDTO;

import java.util.List;
import java.util.Map;

public interface IMemberService {

    void joinGroup(String groupId, String userId, String role);

    Map<String, List<MemberDTO>> mapGroupIdToMembers(List<String> groupIds);

    List<String> getConversationIds();

    List<String> getGroupIds();

    Map<String, String> mapFriendIdToGroupId(List<String> friendIds);

    void deleteConversation(String groupId);

    void leaveGroup(String groupId);

    void changeNickname(String userId, String groupId, String nickName);

}
