package com.chat.bee.services;

import com.chat.bee.entities.Attachment;
import com.chat.bee.models.AttachmentDTO;

import java.util.List;
import java.util.Map;

public interface IAttachmentService {

    void save(Attachment attachment);

    Map<String, List<AttachmentDTO>> mapChatIdToAttachments(List<String> chatIds);

}
