package com.chat.bee.services;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IUploadService {

    List<String> uploadFile(List<MultipartFile> multipartFiles);
}
