package com.chat.bee.services;

import com.chat.bee.entities.Attachment;
import com.chat.bee.entities.Chat;
import com.chat.bee.entities.GroupChat;
import com.chat.bee.entities.Mention;
import com.chat.bee.models.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IChatService {

    void save(Chat chat);

    List<ChatDTO> getChats(String groupId, int page);

    GroupChatDetailDTO getGroupChat(String groupId);

    String createGroupChat(List<String> members, GroupChatDTO groupChatDTO);

    void addChat(ChatDTO chatDTO);

    List<GroupChatDTO> getGroupChats(Date lastedUpdate, int type);

    List<String> getGroupIds();

    void changeGroupName(String groupId, String name);

    void addMember(String groupId, List<String> userIds);

    void leaveGroup(String groupId);

    void updateChat(String chatId, String content);

    void deleteChat(String chatId);

    void customizeScreen(String groupId, ChatStyleDTO chatStyleDTO);

    void pinChat(String chatId);
}
