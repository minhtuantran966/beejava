package com.chat.bee.services.impls;

import com.chat.bee.entities.Reaction;
import com.chat.bee.models.ReactionDTO;
import com.chat.bee.models.UserDTO;
import com.chat.bee.repositories.IReactionRepository;
import com.chat.bee.services.IReactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Minh Tuấn
 * 11:20 CH 13/03/2021/03/2021
 */

@Service
public class ReactionService implements IReactionService {

    @Autowired
    private IReactionRepository reactionRepository;

//  --------------------------------------------------------------------------------------------------------------------

    public List<Reaction> getReactionByChatIds(List<String> chatIds) {
        return reactionRepository.findAllByChatIdIn(chatIds);
    }

    @Override
    public Map<String, List<ReactionDTO>> mapChatIdToReactions(List<Reaction> reactions, Map<String, UserDTO> mapUser) {
        Map<String, List<ReactionDTO>> reactionMap = new HashMap<>();
        for (Reaction reaction : reactions) {
            String chatId = reaction.getChatId();
            reactionMap.computeIfAbsent(chatId, k -> new ArrayList<>());
            reactionMap.get(chatId).add(toDTo(reaction, mapUser));
        }
        return reactionMap;
    }

//  --------------------------------------------------------------------------------------------------------------------

    private ReactionDTO toDTo(Reaction reaction, Map<String, UserDTO> mapUser) {
        ReactionDTO reactionDTO = new ReactionDTO();
        reactionDTO.setType(reaction.getType());
        reactionDTO.setCount(reaction.getCount());
        reactionDTO.setUser(mapUser.get(reaction.getUserId()));
        return reactionDTO;
    }
}
