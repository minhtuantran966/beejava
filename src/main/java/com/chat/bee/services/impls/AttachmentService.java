package com.chat.bee.services.impls;

import com.chat.bee.converter.AttachmentConverter;
import com.chat.bee.entities.Attachment;
import com.chat.bee.models.AttachmentDTO;
import com.chat.bee.repositories.IAttachmentRepository;
import com.chat.bee.services.IAttachmentService;
import com.chat.bee.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Minh Tuấn
 * 10:44 CH 13/03/2021/03/2021
 */

@Service
public class AttachmentService implements IAttachmentService {

    @Autowired
    private IAttachmentRepository attachmentRepository;

    @Autowired
    private AttachmentConverter attachmentConverter;

//  --------------------------------------------------------------------------------------------------------------------

    @Override
    public void save(Attachment attachment) {
        attachmentRepository.save(attachment);
    }

    @Override
    public Map<String, List<AttachmentDTO>> mapChatIdToAttachments(List<String> chatIds) {
        List<Attachment> attachments = attachmentRepository.findAllByChatIdIn(chatIds);
        Map<String, List<AttachmentDTO>> attachmentMap = new HashMap<>();
        for (Attachment attachment : attachments) {
            String id = attachment.getChatId();
            attachmentMap.computeIfAbsent(id, k -> new ArrayList<>());
            attachmentMap.get(id).add(attachmentConverter.toDTO(attachment));
        }
        return attachmentMap;
    }
}
