package com.chat.bee.services.impls;

import com.chat.bee.converter.ChatStyleConverter;
import com.chat.bee.entities.*;
import com.chat.bee.models.*;
import com.chat.bee.repositories.IChatRepository;
import com.chat.bee.repositories.IChatStyleRepository;
import com.chat.bee.repositories.IGroupChatRepository;
import com.chat.bee.services.*;
import com.chat.bee.utils.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ChatService implements IChatService {

    @Autowired
    private IChatRepository chatRepository;

    @Autowired
    private IGroupChatRepository groupChatRepository;

    @Autowired
    private IAttachmentService attachmentService;

    @Autowired
    private IReactionService reactionService;

    @Autowired
    private IMentionService mentionService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IMemberService memberService;

    @Autowired
    private SocketUtils socketUtils;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ChatStyleConverter chatStyleConverter;

    @Autowired
    private IChatStyleRepository chatStyleRepository;

//  --------------------------------------------------------------------------------------------------------------------

    @Override
    public void save(Chat chat) {
        if (StringUtils.isBlankOrNull(chat.getId())) {
            chat.setId(StringUtils.generateId(chat.getGroupId()));
        }
        chatRepository.save(chat);
    }

    @Override
    public List<ChatDTO> getChats(String groupId, int page) {
        Sort sort = Sort.by(EnumConst.SortFieldEnum.time.toString()).descending();
        Pageable pageable = PageRequest.of(page, ContainsUtils.CHAT_PER_PAGE, sort);
        List<Chat> chats = chatRepository.findChatByGroupId(groupId, pageable);
        return buildChat(chats);
    }

    @Override
    public GroupChatDetailDTO getGroupChat(String groupId) {
        GroupChat groupChat = groupChatRepository.findGroupChatById(groupId);
        return buildGroupChatDetail(groupChat);
    }

    @Override
    public String createGroupChat(List<String> members, GroupChatDTO groupChatDTO) {
        String myId = UserUtils.getCurrentUserId();
        String groupId = StringUtils.generateId(myId);
        if (groupChatDTO == null) {
            groupChatDTO = new GroupChatDTO();
        }
        if (members.size() == 1) {
            groupChatDTO.setType(EnumConst.GroupChatTypeEnum.FRIEND.toString());
        }
        joinGroupChat(members, groupId);
        Chat chat = createSystemChat(groupId, EnumConst.NotificationTypeEnum.CONTACTED.toString(), null, null);
        GroupChat groupChat = GroupChat.builder()
                .id(groupId)
                .createBy(myId).createTime(new Date())
                .name(groupChatDTO.getName())
                .type(groupChatDTO.getType())
                .avatar(groupChatDTO.getAvatar())
                .lastedChat(chat)
                .build();
        chatRepository.save(chat);
        groupChatRepository.save(groupChat);
        return groupId;
    }

    @Override
    public void addChat(ChatDTO chatDTO) {
        String myId = UserUtils.getCurrentUserId();
        String groupId = chatDTO.getGroupId();
        String chatId = StringUtils.generateId(groupId);
        String chatType = chatDTO.getType();
        if (StringUtils.isBlankOrNull(chatType))
            chatType = EnumConst.ChatTypeEnum.CHAT.toString();

//      Chat
        Chat chat = Chat.builder()
                .id(chatId)
                .chatBy(myId)
                .type(chatType)
                .content(chatDTO.getContent())
                .time(new Date()).groupId(groupId)
                .parentId(chatDTO.getParentChat() == null ? null : chatDTO.getParentChat().getId())
                .edit(false).pin(false).build();
        save(chat);

//      update lasted chat
        GroupChat groupChat = groupChatRepository.findGroupChatById(groupId);
        groupChat.setLastedChat(chat);
        groupChat.setLastedUpdate(chat.getTime());
        groupChatRepository.save(groupChat);

//      Attachment
        if (chatDTO.getAttachments() != null) {
            Tika tika = new Tika();

            for (AttachmentDTO attachmentDTO : chatDTO.getAttachments()) {

                String url = attachmentDTO.getLink();
                String mimeType = "";
                if (attachmentDTO.getType() == null) {
                    mimeType = tika.detect(url);
                }

                Attachment attachment = Attachment.builder().id(StringUtils.generateId(chatId)).chatId(chatId)
                        .groupId(groupId).type(getTypeByMiniType(mimeType)).link(url)
                        .duration(attachmentDTO.getDuration()).title(attachmentDTO.getTitle())
                        .iconLink(attachmentDTO.getIconLink()).description(attachmentDTO.getDescription())
                        .imageLink(attachmentDTO.getImageLink()).build();
                attachmentService.save(attachment);
            }
        }

//      Mention
        if (chatDTO.getMentionUsers() != null) {
            for (UserDTO mentionUser : chatDTO.getMentionUsers()) {
                Mention mention = Mention.builder().id(StringUtils.generateId(chatId)).chatId(chatId)
                        .userId(mentionUser.getId()).build();
                mentionService.save(mention);
            }
        }

        emit(EnumConst.SocketListener.NEW_CHAT.toString(), buildChat(Collections.singletonList(chat)).get(0));
    }

    @Override
    public List<GroupChatDTO> getGroupChats(Date lastedUpdate, int type) {
        lastedUpdate = null;
        Sort sort = Sort.by(EnumConst.SortFieldEnum.time.toString()).descending();
        Pageable pageable = PageRequest.of(0, ContainsUtils.GROUP_CHAT_PER_PAGE, sort);

        List<String> groupChatIds = memberService.getConversationIds();
        List<String> types = Arrays.stream(EnumConst.GroupChatTypeEnum.values()).map(Enum::toString).collect(Collectors.toList());
        if (type != 1) {
            types.remove(EnumConst.GroupChatTypeEnum.FRIEND.toString());
        }
        List<GroupChat> groupChats;
        if (lastedUpdate == null) {
            groupChats = groupChatRepository.findAllByIdInAndTypeInOrderByLastedUpdateDesc(groupChatIds, types, pageable);
        } else {
            groupChats = groupChatRepository
                    .findAllByIdInAndTypeInAndLastedUpdateIsAfterOrderByLastedUpdateDesc(groupChatIds, types, lastedUpdate, pageable);
        }
        return buildGroupChats(groupChats);
    }

    @Override
    public List<String> getGroupIds() {
        return memberService.getGroupIds();
    }

    @Override
    public void changeGroupName(String groupId, String name) {
        GroupChat groupChat = groupChatRepository.findGroupChatById(groupId);
        groupChat.setName(name);
        groupChatRepository.save(groupChat);
    }

    @Override
    public void addMember(String groupId, List<String> userIds) {
        for (String userId : userIds) {
            memberService.joinGroup(groupId, userId, EnumConst.UserRoleEnum.USER.toString());
        }
    }

    @Override
    public void leaveGroup(String groupId) {
        memberService.leaveGroup(groupId);
    }

    @Override
    public void updateChat(String chatId, String content) {
        Optional<Chat> optionalChat = chatRepository.findById(chatId);
        if (optionalChat.isPresent()) {
            Chat chat = optionalChat.get();
            chat.setContent(content);
            chat.setEdit(true);
            chatRepository.save(chat);
        }
    }

    @Override
    public void deleteChat(String chatId) {
        Optional<Chat> optionalChat = chatRepository.findById(chatId);
        if (optionalChat.isPresent()) {
            emit(EnumConst.SocketListener.REMOVE_CHAT.toString(), optionalChat.get().getGroupId(), chatId);
            chatRepository.deleteById(chatId);
        }
    }

    @Override
    public void customizeScreen(String groupId, ChatStyleDTO chatStyleDTO) {
        Optional<ChatStyle> optionalChatStyle = chatStyleRepository.findById(groupId);
        if (optionalChatStyle.isPresent()) {
            ChatStyle chatStyle = optionalChatStyle.get();
            chatStyle.setBackground(chatStyleDTO.getBackground());
            chatStyle.setColor(chatStyleDTO.getColor());
            chatStyle.setEmoji(chatStyleDTO.getEmoji());
            chatStyleRepository.save(chatStyle);
        }
    }

    @Override
    public void pinChat(String chatId) {
        Optional<Chat> optionalChat = chatRepository.findById(chatId);
        if (optionalChat.isPresent()) {
            String type = EnumConst.NotificationTypeEnum.PIN_CHAT.toString();
            Chat chat = optionalChat.get();
            if (chat.isPin()) {
                type = EnumConst.NotificationTypeEnum.UP_PIN_CHAT.toString();
            }
            chat.setPin(!chat.isPin());
            chatRepository.save(chat);
            String groupId = chat.getGroupId();
            Chat systemChat = createSystemChat(groupId, type, UserUtils.getCurrentUserId(), null);
            chatRepository.save(systemChat);
            emit(EnumConst.SocketListener.PIN_CHAT.toString(), groupId, chatId);
            emit(EnumConst.SocketListener.NEW_CHAT.toString(), buildChat(Collections.singletonList(systemChat)).get(0));
        }
    }

//  --------------------------------------------------------------------------------------------------------------------

    private List<ChatDTO> buildChat(List<Chat> chats) {

        List<Chat> chatFull = new ArrayList<>(chats);
        List<Chat> parentChat = getParentChats(chats);
        chatFull.addAll(parentChat);

        List<String> chatIds = toListId(chatFull);
        Map<String, List<AttachmentDTO>> attachmentMap = attachmentService.mapChatIdToAttachments(chatIds);
        List<Reaction> reactions = reactionService.getReactionByChatIds(chatIds);
        List<Mention> mentions = mentionService.getMentionsByChatIds(chatIds);

        List<String> userIds = getUserId(chatFull, reactions, mentions);
        for (Chat chat : chats) {
            ChatNotification chatNotification = chat.getNotification();
            if (chatNotification != null) {
                String fromId = chatNotification.getFromId();
                String toId = chatNotification.getToId();
                if (fromId != null) {
                    userIds.add(fromId);
                }
                if (toId != null) {
                    userIds.add(toId);
                }
            }
        }
        Map<String, UserDTO> userMap = userService.getMapUser(userIds);

        Map<String, List<ReactionDTO>> reactionMap = reactionService.mapChatIdToReactions(reactions, userMap);
        Map<String, List<UserDTO>> mentionMap = mentionService.mapChatIdToMentionUsers(mentions, userMap);

        Map<String, ChatDTO> chatMap = new HashMap<>();
        for (Chat chat : chatFull) {
            String id = chat.getId();
            ChatDTO chatDTO = new ChatDTO();
            chatDTO.setId(id);
            chatDTO.setGroupId(chat.getGroupId());
            chatDTO.setType(chat.getType());
            chatDTO.setChatTime(chat.getTime());
            chatDTO.setContent(chat.getContent());
            chatDTO.setPin(chat.isPin());
            chatDTO.setSystem(isSystem(chat));
            chatDTO.setAttachments(attachmentMap.get(id));
            chatDTO.setChatBy(userMap.get(chat.getChatBy()));
            chatDTO.setReactions(reactionMap.get(id));
            chatDTO.setMentionUsers(mentionMap.get(id));
            ChatNotification chatNotification = chat.getNotification();
            if (chatNotification != null) {
                ChatNotificationDTO chatNotificationDTO = ChatNotificationDTO.builder()
                        .type(chatNotification.getType())
                        .from(userMap.get(chatNotification.getFromId()))
                        .to(userMap.get(chatNotification.getToId()))
                        .build();
                chatDTO.setNotification(chatNotificationDTO);
            }

            chatMap.put(id, chatDTO);
        }

        List<ChatDTO> chatDTOS = new ArrayList<>();
        for (Chat chat : chats) {
            String id = chat.getId();
            ChatDTO chatDTO = chatMap.get(id);
            chatDTO.setParentChat(chatMap.get(chat.getParentId()));
            chatDTOS.add(chatDTO);
        }
        return chatDTOS;
    }

    private List<GroupChatDTO> buildGroupChats(List<GroupChat> groupChats) {
        List<GroupChatDTO> groupChatDTOS = new ArrayList<>();

        List<String> groupIds = groupChats.stream().map(GroupChat::getId).collect(Collectors.toList());
        Map<String, List<MemberDTO>> mapGroupIdToMembers = memberService.mapGroupIdToMembers(groupIds);

        List<Chat> chats = groupChats.stream().map(GroupChat::getLastedChat).collect(Collectors.toList());
        chats.removeIf(Objects::isNull);
        List<ChatDTO> chatDTOS = buildChat(chats);
        Map<String, ChatDTO> chatDTOMap = chatDTOS.stream().collect(Collectors.toMap(ChatDTO::getId, k -> k));

        List<ChatStyle> chatStyles = chatStyleRepository.findAllByGroupIdIn(groupIds);
        Map<String, ChatStyle> chatStyleMap = chatStyles.stream().collect(Collectors.toMap(ChatStyle::getGroupId, c -> c));

        for (GroupChat groupChat : groupChats) {
            String groupId = groupChat.getId();
            String chatId = groupChat.getLastedChat() == null ? null : groupChat.getLastedChat().getId();
            List<MemberDTO> members = mapGroupIdToMembers.get(groupId);
            String type = groupChat.getType();
            if (groupChat.getName() == null) groupChat.setName("");
            StringBuilder name = new StringBuilder(groupChat.getName());
            String avatar = groupChat.getAvatar();
            if (StringUtils.isBlankOrNull(name.toString())) {
                name = new StringBuilder();
                for (MemberDTO member : members) {
                    if (member.getUser().getId().equals(UserUtils.getCurrentUserId())) continue;
                    name.append(member.getUser().getLastName()).append(", ");
                    if (type.equals(EnumConst.GroupChatTypeEnum.FRIEND.toString()))
                        avatar = member.getUser().getAvatarUrl();
                }
                name = new StringBuilder(name.substring(0, name.length() - 2));
            }
            GroupChatDTO groupChatDTO = GroupChatDTO.builder()
                    .id(groupId)
                    .createBy(groupChat.getCreateBy())
                    .createTime(groupChat.getCreateTime())
                    .name(name.toString())
                    .type(type)
                    .lastedChat(chatDTOMap.get(chatId))
                    .avatar(avatar)
                    .members(members)
                    .chatStyle(chatStyleConverter.toDTO(chatStyleMap.get(groupId)))
                    .build();
            groupChatDTOS.add(groupChatDTO);
        }
        return groupChatDTOS;
    }

    private GroupChatDetailDTO buildGroupChatDetail(GroupChat groupChat) {

        String groupId = groupChat.getId();

        List<ChatDTO> chats = getChats(groupId, 0);
        List<ChatDTO> pinChats = chats.stream().filter(ChatDTO::isPin).collect(Collectors.toList());

        Map<String, List<MemberDTO>> mapGroupIdToMembers =
                memberService.mapGroupIdToMembers(Collections.singletonList(groupId));

        List<MemberDTO> members = mapGroupIdToMembers.get(groupId);

        StringBuilder name = new StringBuilder();
        if (!StringUtils.isBlankOrNull(groupChat.getName())) {
            name.append(groupChat.getName());
        }

        if (StringUtils.isBlankOrNull(name.toString())) {
            name = new StringBuilder();
            for (MemberDTO member : members) {
                if (member.getUser().getId().equals(UserUtils.getCurrentUserId())) continue;
                name.append(member.getUser().getLastName()).append(", ");
            }
            name = new StringBuilder(name.substring(0, name.length() - 2));
        }

        GroupChatDTO groupChatDTO = GroupChatDTO.builder()
                .id(groupId)
                .createBy(groupChat.getCreateBy())
                .createTime(groupChat.getCreateTime())
                .name(name.toString())
                .type(groupChat.getType())
                .lastedChat(chats.size() == 0 ? null : chats.get(0))
                .members(members)
                .build();

        GroupChatDetailDTO groupChatDetail = new GroupChatDetailDTO();
        groupChatDetail.setGroupChat(groupChatDTO);
        groupChatDetail.setChats(chats);
        groupChatDetail.setPinChats(pinChats);

        return groupChatDetail;
    }

    private List<String> getUserId(List<Chat> chats, List<Reaction> reactions, List<Mention> mentions) {
        Set<String> userIds = new HashSet<>();
        for (Chat chat : chats) {
            userIds.add(chat.getChatBy());
        }
        for (Reaction reaction : reactions) {
            userIds.add(reaction.getUserId());
        }
        for (Mention mention : mentions) {
            userIds.add(mention.getUserId());
        }
        return new ArrayList<>(userIds);
    }

    private List<String> toListId(List<Chat> chats) {
        List<String> ids = new ArrayList<>();
        for (Chat chat : chats) {
            ids.add(chat.getId());
        }
        return ids;
    }

    private boolean isSystem(Chat chat) {
        return ContainsUtils.systemChatType().contains(chat.getType());
    }

    private List<Chat> getParentChats(List<Chat> chats) {
        List<String> parentIds = new ArrayList<>();
        for (Chat chat : chats) {
            String parentId = chat.getParentId();
            if (!StringUtils.isBlankOrNull(parentId)) {
                parentIds.add(parentId);
            }
        }
        return parentIds.size() == 0 ? new ArrayList<>() : chatRepository.findAllByIdIn(parentIds);
    }

    private String getTypeByMiniType(String mineType) {
        if (StringUtils.isBlankOrNull(mineType)) {
            return EnumConst.AttachmentTypeEnum.LINK.toString();
        }
        if (mineType.contains("image")) {
            return EnumConst.AttachmentTypeEnum.IMAGE.toString();
        }
        if (mineType.contains("video")) {
            return EnumConst.AttachmentTypeEnum.VIDEO.toString();
        }
        if (mineType.contains("audio")) {
            return EnumConst.AttachmentTypeEnum.AUDIO.toString();
        }
        return EnumConst.AttachmentTypeEnum.FILE.toString();
    }

    private void joinGroupChat(List<String> memberIds, String groupId) {
        for (String memberId : memberIds) {
            memberService.joinGroup(groupId, memberId, EnumConst.UserRoleEnum.USER.toString());
        }
        memberService.joinGroup(groupId, UserUtils.getCurrentUserId(), EnumConst.UserRoleEnum.ADMIN.toString());
    }

    private Chat createSystemChat(String groupId, String type, String fromId, String toId) {
        return Chat.builder()
                .notification(ChatNotification.builder()
                        .type(type)
                        .fromId(fromId)
                        .toId(toId)
                        .build())
                .groupId(groupId)
                .id(StringUtils.generateId(groupId))
                .time(new Date())
                .type(EnumConst.ChatTypeEnum.NOTIFICATION.toString())
                .build();
    }

    private void emit(String event, Object... data) {
        if (data.length > 1) {
            socketUtils.getSocket().emit(event, data);
            return;
        }
        try {
            String jsonData = objectMapper.writeValueAsString(data[0]);
            socketUtils.getSocket().emit(event, jsonData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
