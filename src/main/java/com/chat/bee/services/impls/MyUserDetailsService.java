package com.chat.bee.services.impls;

import com.chat.bee.entities.UserLogin;
import com.chat.bee.models.MyUser;
import com.chat.bee.models.UserDTO;
import com.chat.bee.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserLogin userLogin = userService.getUserLogin(username);
        if (userLogin == null || userLogin.getId() == null) {
            throw new UsernameNotFoundException(username);
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(userLogin.getRole()));

        UserDTO userDTO = userService.getUserById(userLogin.getUserId());

        MyUser myUser = new MyUser(username, userLogin.getPassword(), true, true,
                true, true, authorities);
        myUser.setUser(userDTO);
        return myUser;
    }
}
