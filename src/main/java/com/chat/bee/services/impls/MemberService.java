package com.chat.bee.services.impls;

import com.chat.bee.entities.ConversationMember;
import com.chat.bee.entities.GroupMember;
import com.chat.bee.entities.User;
import com.chat.bee.models.MemberDTO;
import com.chat.bee.models.UserDTO;
import com.chat.bee.repositories.IConversationMemberRepository;
import com.chat.bee.repositories.IGroupMemberRepository;
import com.chat.bee.services.IMemberService;
import com.chat.bee.services.IUserService;
import com.chat.bee.utils.StringUtils;
import com.chat.bee.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Minh Tuấn
 * 12:48 SA 14/03/2021/03/2021
 */

@Service
public class MemberService implements IMemberService {

    @Autowired
    private IGroupMemberRepository groupMemberRepository;

    @Autowired
    private IConversationMemberRepository conversationMemberRepository;

    @Autowired
    private IUserService userService;

//  --------------------------------------------------------------------------------------------------------------------

    @Override
    public void joinGroup(String groupId, String userId, String role) {
        GroupMember groupMember = new GroupMember();
        groupMember.setId(StringUtils.generateId(groupId));
        groupMember.setGroupId(groupId);
        groupMember.setLastedSeen(new Date());
        groupMember.setRole(role);
        groupMember.setUserId(userId);
        groupMemberRepository.save(groupMember);

        ConversationMember conversationMember = new ConversationMember();
        conversationMember.setId(StringUtils.generateId(groupId));
        conversationMember.setGroupId(groupId);
        conversationMember.setUserId(userId);
        conversationMember.setTime(new Date());
        conversationMemberRepository.save(conversationMember);
    }

    @Override
    public Map<String, List<MemberDTO>> mapGroupIdToMembers(List<String> groupIds) {
        List<GroupMember> groupMembers = groupMemberRepository.findAllByGroupIdIn(groupIds);

        List<String> userIds = groupMembers.stream().map(GroupMember::getUserId).collect(Collectors.toList());

        Map<String, UserDTO> userMap = userService.getMapUser(userIds);

        Map<String, List<MemberDTO>> map = new HashMap<>();

        for (GroupMember groupMember : groupMembers) {
            MemberDTO memberDTO = new MemberDTO();
            memberDTO.setLastedSeen(groupMember.getLastedSeen());
            memberDTO.setRole(groupMember.getRole());
            memberDTO.setUser(userMap.get(groupMember.getUserId()));
            memberDTO.setNickName(groupMember.getNickname());

            String groupId = groupMember.getGroupId();

            map.computeIfAbsent(groupId, k -> new ArrayList<>());
            map.get(groupId).add(memberDTO);
        }

        return map;
    }

    @Override
    public List<String> getConversationIds() {
        List<ConversationMember> conversationMembers =
                conversationMemberRepository.findAllByUserId(UserUtils.getCurrentUserId());
        return conversationMembers.stream().map(ConversationMember::getGroupId).collect(Collectors.toList());
    }

    @Override
    public List<String> getGroupIds() {
        List<GroupMember> groupMembers = groupMemberRepository.findAllByUserId(UserUtils.getCurrentUserId());
        return groupMembers.stream().map(GroupMember::getGroupId).collect(Collectors.toList());
    }

    @Override
    public Map<String, String> mapFriendIdToGroupId(List<String> friendIds) {
        String myId = UserUtils.getCurrentUserId();
        friendIds.add(myId);
        List<GroupMember> groupMembers = groupMemberRepository.findAllByUserIdIn(friendIds);
        Map<String, List<String>> mapGroupIdToMemberIds = new HashMap<>();
        for (GroupMember groupMember : groupMembers) {
            String groupId = groupMember.getGroupId();
            mapGroupIdToMemberIds.computeIfAbsent(groupId, k -> new ArrayList<>());
            mapGroupIdToMemberIds.get(groupId).add(groupMember.getUserId());
        }

        Map<String, String> mapValue = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : mapGroupIdToMemberIds.entrySet()) {
            List<String> memberIds = entry.getValue();
            if (memberIds.size() == 2 && memberIds.contains(myId)) {
                memberIds.removeIf(s -> s.equals(myId));
                mapValue.put(memberIds.get(0), entry.getKey());
            }
        }
        return mapValue;
    }

    @Override
    public void deleteConversation(String groupId) {
        conversationMemberRepository.deleteByGroupIdAndUserId(groupId, UserUtils.getCurrentUserId());
    }

    @Override
    public void leaveGroup(String groupId) {
        groupMemberRepository.deleteByGroupIdAndUserId(groupId, UserUtils.getCurrentUserId());
        conversationMemberRepository.deleteByGroupIdAndUserId(groupId, UserUtils.getCurrentUserId());
    }

    @Override
    public void changeNickname(String userId, String groupId, String nickName) {
        GroupMember groupMember = groupMemberRepository.findAllByGroupIdAndUserId(groupId, userId);
        groupMember.setNickname(nickName);
        groupMemberRepository.save(groupMember);
    }

}
