package com.chat.bee.services.impls;

import com.chat.bee.converter.UserConverter;
import com.chat.bee.entities.Friend;
import com.chat.bee.entities.InviteRequest;
import com.chat.bee.entities.User;
import com.chat.bee.entities.UserLogin;
import com.chat.bee.models.UserDTO;
import com.chat.bee.repositories.IFriendRepository;
import com.chat.bee.repositories.IInviteRequestRepository;
import com.chat.bee.repositories.IUserLoginRepository;
import com.chat.bee.repositories.IUserRepository;
import com.chat.bee.services.IChatService;
import com.chat.bee.services.IMemberService;
import com.chat.bee.services.IUserService;
import com.chat.bee.utils.EnumConst;
import com.chat.bee.utils.StringUtils;
import com.chat.bee.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Minh Tuấn
 * 11:44 CH 13/03/2021/03/2021
 */

@Service
public class UserService implements IUserService {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserLoginRepository userLoginRepository;

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private IInviteRequestRepository inviteRequestRepository;

    @Autowired
    private IFriendRepository friendRepository;

    @Autowired
    private IChatService chatService;

    @Autowired
    private IMemberService memberService;

//  --------------------------------------------------------------------------------------------------------------------

    @Override
    public boolean phoneIsExits(String countryCode, String phone) {
        return userLoginRepository.findUserLoginByCountryCodeAndPhone(countryCode, phone) != null;
    }

    @Override
    public Map<String, UserDTO> getMapUser(List<String> userIds) {
        List<User> users = userRepository.findAllByIdIn(userIds);
        Map<String, UserDTO> userMap = new HashMap<>();
        for (User user : users) {
            userMap.put(user.getId(), userConverter.toDTO(user));
        }
        return userMap;
    }

    @Override
    public UserDTO createChatUser(UserDTO userDTO, String password) {

        String phone = userDTO.getPhone();

        User user = User.builder()
                .id(StringUtils.generateId(phone))
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .fullNameMod(StringUtils.getNameMod(userDTO.getFirstName(), userDTO.getLastName()))
                .gender(userDTO.getGender())
                .birthday(userDTO.getBirthday())
                .countryCode(userDTO.getCountryCode())
                .phone(phone)
                .avatarUrl(userDTO.getAvatarUrl())
                .online(true)
                .lastActive(new Date())
                .build();
        String userId = save(user);

        UserLogin userLogin = new UserLogin(StringUtils.generateId(phone), userDTO.getCountryCode(), phone,
                null, bCryptPasswordEncoder.encode(password), userId, EnumConst.UserRoleEnum.USER.toString());
        save(userLogin);
        return userConverter.toDTO(user);
    }

    @Override
    public UserLogin getUserLogin(String username) {
        return userLoginRepository.findUserLoginByPhoneOrUsernameIs(username);
    }

    @Override
    public UserDTO getUserById(String userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        return optionalUser.map(user -> userConverter.toDTO(user)).orElse(null);
    }

    @Override
    public List<UserDTO> searchUser(String query) {
        List<User> users = userRepository.findAllByPhoneLikeOrFirstNameLikeOrLastNameLike(query);
        return buildUser(users);
    }

    @Override
    public void addRequest(String friendId, String groupId) {
        InviteRequest inviteRequest = new InviteRequest();
        String myId = UserUtils.getCurrentUserId();
        inviteRequest.setId(StringUtils.generateId(myId));
        inviteRequest.setFromId(myId);
        inviteRequest.setToId(friendId);
        inviteRequest.setGroupId(groupId);
        inviteRequestRepository.save(inviteRequest);
    }

    @Override
    public void removeRequest(String friendId, String groupId) {
        inviteRequestRepository.removeByFromIdAndToIdAndGroupId(UserUtils.getCurrentUserId(), friendId, groupId);
    }

    @Override
    public String acceptRequest(String friendId, String groupId) {
        Friend friend = new Friend();
        friend.setId(StringUtils.generateId(friendId));
        friend.setFromId(UserUtils.getCurrentUserId());
        friend.setToId(friendId);
        friendRepository.save(friend);
        inviteRequestRepository.removeByFromIdAndToIdAndGroupId(friendId, UserUtils.getCurrentUserId(), groupId);
        if (StringUtils.isBlankOrNull(groupId)) {
            groupId = chatService.createGroupChat(Collections.singletonList(friendId), null);
        }
        return groupId;
    }

    @Override
    public void rejectRequest(String friendId, String groupId) {
        inviteRequestRepository.removeByFromIdAndToIdAndGroupId(friendId, UserUtils.getCurrentUserId(), groupId);
    }

    @Override
    public void resetPassword(String countryCode, String phone, String password) {
        UserLogin userLogin = userLoginRepository.findUserLoginByCountryCodeAndPhone(countryCode, phone);
        userLogin.setPassword(bCryptPasswordEncoder.encode(password));
        userLoginRepository.save(userLogin);
    }

    @Override
    public boolean changePassword(String oldPassword, String newPassword) {
        UserLogin userLogin = userLoginRepository.findByUserId(UserUtils.getCurrentUserId());
        if (bCryptPasswordEncoder.matches(oldPassword, userLogin.getPassword())) {
            userLogin.setPassword(bCryptPasswordEncoder.encode(newPassword));
            userLoginRepository.save(userLogin);
            return true;
        }
        return false;
    }

    @Override
    public void updateMyInfo(UserDTO userDTO) {
        Optional<User> optionalUser = userRepository.findById(userDTO.getId());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setGender(userDTO.getGender());
            user.setBirthday(userDTO.getBirthday());
            user.setAvatarUrl(userDTO.getAvatarUrl());
            userRepository.save(user);
        }
    }

    @Override
    public List<UserDTO> friendList() {
        String myId = UserUtils.getCurrentUserId();
        List<Friend> friends = friendRepository.findAllByFromIdInAOrToIdIn(Collections.singletonList(myId));
        List<String> friendIds = friends.stream().map(Friend::getFromId).collect(Collectors.toList());
        friendIds.addAll(friends.stream().map(Friend::getToId).collect(Collectors.toList()));
        friendIds.removeIf(s -> s.equals(myId));
        List<User> users = userRepository.findAllByIdIn(friendIds);
        return buildUser(users);
    }

    @Override
    public List<UserDTO> requestContactList() {
        List<InviteRequest> myRequestReceive = inviteRequestRepository
                .findAllByToIdAndGroupId(UserUtils.getCurrentUserId(), null);
        List<String> userIds = myRequestReceive.stream().map(InviteRequest::getFromId).collect(Collectors.toList());
        List<User> users = userRepository.findAllByIdIn(userIds);
        return buildUser(users);
    }

    @Override
    public List<UserDTO> getUserByPhone(List<String> phones) {
        List<User> users = userRepository.findAllByPhoneIn(phones);
        return buildUser(users);
    }

//  --------------------------------------------------------------------------------------------------------------------

    private String save(User user) {
        if (StringUtils.isBlankOrNull(user.getId())) {
            user.setId(StringUtils.generateId(user.getPhone()));
        }
        userRepository.save(user);
        return user.getId();
    }

    private void save(UserLogin userLogin) {
        if (StringUtils.isBlankOrNull(userLogin.getId())) {
            userLogin.setId(StringUtils.generateId(userLogin.getPhone()));
        }
        userLoginRepository.save(userLogin);
    }

    private List<UserDTO> buildUser(List<User> users) {
        List<UserDTO> userDTOS = new ArrayList<>();

        List<String> userIds = users.stream().map(User::getId).collect(Collectors.toList());
        String myId = UserUtils.getCurrentUserId();

//      build my request
        List<InviteRequest> myRequest = inviteRequestRepository.findAllByFromIdInAndToIdInAndGroupId(Collections.singletonList(myId),
                userIds, null);
        List<String> userHasRequestByMe = myRequest.stream().map(InviteRequest::getToId).collect(Collectors.toList());

//      build my request receive
        List<InviteRequest> myRequestReceive = inviteRequestRepository.findAllByFromIdInAndToIdInAndGroupId(userIds,
                Collections.singletonList(myId), null);
        List<String> userRequestMe = myRequestReceive.stream().map(InviteRequest::getFromId).collect(Collectors.toList());

        userIds.add(myId);

//      danh sách bạn bè
        List<Friend> friends = friendRepository.findAllByFromIdInAndToIdIn(userIds);
        List<String> friendIds = friends.stream().map(Friend::getFromId).collect(Collectors.toList());
        friendIds.addAll(friends.stream().map(Friend::getToId).collect(Collectors.toList()));
        friendIds.removeIf(s -> s.equals(myId));

//      map groupId
        Map<String, String> mapFriendIdToGroupId = memberService.mapFriendIdToGroupId(friendIds);

//      build groupId
        for (User user : users) {
            String userId = user.getId();
            UserDTO userDTO = userConverter.toDTO(user);
            userDTO.setFriend(friendIds.contains(userId));
            userDTO.setRequested(userHasRequestByMe.contains(userId));
            userDTO.setReceiveRequest(userRequestMe.contains(userId));
            userDTO.setGroupId(mapFriendIdToGroupId.get(userId));
            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

}
