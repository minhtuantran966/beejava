package com.chat.bee.services.impls;

import com.chat.bee.entities.Mention;
import com.chat.bee.models.UserDTO;
import com.chat.bee.repositories.IMentionRepository;
import com.chat.bee.services.IMentionService;
import com.chat.bee.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Minh Tuấn
 * 11:27 CH 13/03/2021/03/2021
 */

@Service
public class MentionService implements IMentionService {

    @Autowired
    private IMentionRepository mentionRepository;

//  --------------------------------------------------------------------------------------------------------------------

    @Override
    public void save(Mention mention) {
        if (StringUtils.isBlankOrNull(mention.getId())) {
            mention.setId(StringUtils.generateId(mention.getChatId()));
        }
        mentionRepository.save(mention);
    }

    @Override
    public List<Mention> getMentionsByChatIds(List<String> chatIds) {
        return mentionRepository.findAllByChatIdIn(chatIds);
    }

    @Override
    public Map<String, List<UserDTO>> mapChatIdToMentionUsers(List<Mention> mentions, Map<String, UserDTO> userMap) {
        Map<String, List<UserDTO>> mentionUserMap = new HashMap<>();
        for (Mention mention : mentions) {
            String chatId = mention.getChatId();
            mentionUserMap.computeIfAbsent(chatId, k -> new ArrayList<>());
            mentionUserMap.get(chatId).add(userMap.get(mention.getUserId()));
        }
        return mentionUserMap;
    }

}
