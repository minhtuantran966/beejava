package com.chat.bee.services.impls;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.chat.bee.services.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Minh Tuấn
 * 1:30 SA 09/04/2021
 */

@Service
public class UploadService implements IUploadService {

    @Autowired
    private AmazonS3 amazonS3;

    @Value("${s3.bucketName}")
    private String bucketName;

    @Override
    public List<String> uploadFile(List<MultipartFile> multipartFiles) {
        List<String> urls = new ArrayList<>();
        for (MultipartFile multipartFile : multipartFiles) {
            String fileName = System.currentTimeMillis() + "-" + multipartFile.getOriginalFilename();
            urls.add(getUrl(fileName));
            uploadFile(multipartFile, fileName);
        }
        return urls;
    }

    private String getUrl(String fileName) {
        return "https://" + bucketName + ".s3-hcm-r1.longvan.net/" + fileName;
    }

    private File convertMultiPartFileToFile(final MultipartFile multipartFile) {
        final File file = new File(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        try (final FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(multipartFile.getBytes());
        } catch (final IOException ignored) {

        }
        return file;
    }

    @Async
    void uploadFile(MultipartFile multipartFile, String fileName) {
        try {
            File file = convertMultiPartFileToFile(multipartFile);
            final InputStream stream = new FileInputStream(file);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.length());
            amazonS3.putObject(bucketName, fileName, stream, metadata);
            
//          Set ACL
            AccessControlList newBucketAcl = amazonS3.getBucketAcl(bucketName);
            Grant grant = new Grant(GroupGrantee.AllUsers, Permission.FullControl);
            newBucketAcl.getGrantsAsList().add(grant);
            amazonS3.setObjectAcl(bucketName, fileName, newBucketAcl);

            file.delete();
        } catch (Exception ignored) {
        }
    }

}
