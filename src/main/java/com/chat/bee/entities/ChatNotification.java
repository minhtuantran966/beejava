package com.chat.bee.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Minh Tuấn
 * 11:10 CH 06/03/2021/03/2021
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatNotification {

    private String fromId;
    private String toId;
    private String type;

}
