package com.chat.bee.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Minh Tuấn
 * 11:04 CH 06/03/2021/03/2021
 */

@Data
@Document
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConversationMember {

    @Id
    private String id;
    private String groupId;
    private String userId;
    private Date time;

}
