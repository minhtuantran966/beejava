package com.chat.bee.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Friend {

    @Id
    private String id;
    private String fromId;
    private String toId;
    private String relationship;
    private String memorizeName;

}
