package com.chat.bee.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document
@Builder
public class GroupChat {

    @Id
    private String id;
    private String name;
    private String type;
    private String createBy;
    private Date createTime;
    private String avatar;
    private Chat lastedChat;
    private Date lastedUpdate;

}
