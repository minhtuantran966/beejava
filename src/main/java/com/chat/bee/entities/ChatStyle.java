package com.chat.bee.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class ChatStyle {

    @Id
    private String groupId;
    private String color;
    private String background;
    private String emoji;

}
