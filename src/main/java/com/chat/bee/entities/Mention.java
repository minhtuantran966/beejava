package com.chat.bee.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Minh Tuấn
 * 8:49 CH 13/03/2021/03/2021
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Mention {

    @Id
    private String id;
    private String chatId;
    private String userId;

}
