package com.chat.bee.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Attachment {

    @Id
    private String id;
    private String chatId;
    private String groupId;
    private String type;
    private String link;
    private String title;
    private String iconLink;
    private String description;
    private String imageLink;
    private int duration;

}
