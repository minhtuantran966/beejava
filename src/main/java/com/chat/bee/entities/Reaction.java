package com.chat.bee.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Reaction {

    @Id
    private String id;
    private String chatId;
    private String type;
    private String userId;
    private int count;

}
