package com.chat.bee.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class User {

    @Id
    private String id;
    @TextIndexed(weight = 3)
    private String firstName;
    @TextIndexed(weight = 2)
    private String lastName;
    @TextIndexed(weight = 4)
    private String fullNameMod;
    private String gender;
    private Date birthday;
    private String countryCode;
    @TextIndexed
    private String phone;
    private String avatarUrl;
    private boolean online;
    private Date lastActive;

}
