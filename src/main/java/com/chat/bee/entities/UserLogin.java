package com.chat.bee.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@Document
public class UserLogin {

    @Id
    private String id;
    private String countryCode;
    private String phone;
    private String username;
    private String password;
    private String userId;
    private String role;

}
