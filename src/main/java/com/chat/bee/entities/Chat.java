package com.chat.bee.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Chat {

    @Id
    private String id;
    private String chatBy;
    private String type;
    private String content;
    private Date time;
    private String groupId;
    private String parentId;
    private boolean edit;
    private boolean pin;
    private ChatNotification notification;

}
