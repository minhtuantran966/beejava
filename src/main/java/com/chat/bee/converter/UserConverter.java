package com.chat.bee.converter;

import com.chat.bee.entities.User;
import com.chat.bee.models.UserDTO;
import org.springframework.stereotype.Component;

/**
 * Minh Tuấn
 * 11:51 CH 13/03/2021/03/2021
 */

@Component
public class UserConverter {

    public UserDTO toDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setAvatarUrl(user.getAvatarUrl());
        userDTO.setBirthday(user.getBirthday());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setGender(user.getGender());
        userDTO.setLastActive(user.getLastActive());
        userDTO.setLastName(user.getLastName());
        userDTO.setOnline(user.isOnline());
        userDTO.setPhone(user.getPhone());
        return userDTO;
    }

}
