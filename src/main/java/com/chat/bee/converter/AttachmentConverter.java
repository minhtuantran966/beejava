package com.chat.bee.converter;

import com.chat.bee.entities.Attachment;
import com.chat.bee.models.AttachmentDTO;
import org.springframework.stereotype.Component;

/**
 * Minh Tuấn
 * 10:40 CH 13/03/2021/03/2021
 */

@Component
public class AttachmentConverter {

    public AttachmentDTO toDTO(Attachment attachment) {
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        attachmentDTO.setId(attachment.getId());
        attachmentDTO.setType(attachment.getType());
        attachmentDTO.setLink(attachment.getLink());
        attachmentDTO.setTitle(attachment.getTitle());
        attachmentDTO.setIconLink(attachment.getIconLink());
        attachmentDTO.setDescription(attachment.getDescription());
        attachmentDTO.setImageLink(attachment.getImageLink());
        attachmentDTO.setDuration(attachment.getDuration());
        return attachmentDTO;
    }

}
