package com.chat.bee.converter;

import com.chat.bee.entities.ChatStyle;
import com.chat.bee.models.ChatStyleDTO;
import org.springframework.stereotype.Component;

/**
 * Minh Tuấn
 * 15:34 04/06/2021
 */

@Component
public class ChatStyleConverter {

    public ChatStyleDTO toDTO(ChatStyle chatStyle) {
        if (chatStyle == null) return null;
        return ChatStyleDTO.builder()
                .background(chatStyle.getBackground())
                .color(chatStyle.getColor())
                .emoji(chatStyle.getEmoji())
                .build();
    }
}