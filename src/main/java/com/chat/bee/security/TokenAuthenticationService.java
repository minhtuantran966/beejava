package com.chat.bee.security;

import com.chat.bee.models.UserDTO;
import com.chat.bee.utils.ContainsUtils;
import com.chat.bee.utils.UserUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

public class TokenAuthenticationService {
    static final long EXPIRATIONTIME = 1000 * 60 * 60 * 24 * 10; // 10 days
    static final String SECRET = "2cbnXvk89mLdoopBMkwfCE85YkSp3IQfaAmaAASztVBsg5keM6F2yVkZcpJZuohY" +
            "-sAWegDnUweZaW6psWmwqrLLwdWUh5nv8neJo-zq-bsMSNH79L0CqLIRS5GnNm5qO97El_E6Q" +
            "-lHMN_yrn8Q8iunZEMqfPFZ37JDTNYWmqc";
    static final String TOKEN_PREFIX = "Bearer";
    static final String HEADER_STRING = "Authorization";

    private static ObjectMapper mapper;

    public static void addAuthentication(HttpServletResponse res, UserDTO user) {

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS256.getJcaName());

        String JWT = Jwts.builder()
                .setId(user.getId())
                .setIssuedAt(new Date())
                .setSubject(user.getId())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(signingKey).compact();
        String token = TOKEN_PREFIX + " " + JWT;
        res.addHeader(HEADER_STRING, token);

        UserUtils.setCurrentUserId(user.getId());

        try {
            if (mapper == null) {
                mapper = new ObjectMapper();
                mapper.setDateFormat(new SimpleDateFormat(ContainsUtils.DATE_FORMAT_TYPE));
            }
            res.setCharacterEncoding("UTF-8");
            res.getWriter().write(mapper.writeValueAsString(user));
            res.getWriter().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        String loginId;
        if (token != null) {
            try {
                loginId =
                        Jwts.parserBuilder().setSigningKey(DatatypeConverter.parseBase64Binary(SECRET)).build()
                                .parseClaimsJws(token.replace(TOKEN_PREFIX, "").trim()).getBody()
                                .getSubject();
            } catch (Exception e) {
                return null;
            }
            if (loginId != null) {
                return new UsernamePasswordAuthenticationToken(loginId, null, Collections.emptyList());
            }
        }
        return null;
    }

}
