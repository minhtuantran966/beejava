package com.chat.bee.apis;

import com.chat.bee.entities.Chat;
import com.chat.bee.models.*;
import com.chat.bee.repositories.IChatRepository;
import com.chat.bee.repositories.IGroupChatRepository;
import com.chat.bee.services.IChatService;
import com.chat.bee.services.IMemberService;
import com.chat.bee.services.IUploadService;
import com.chat.bee.services.IUserService;
import com.chat.bee.utils.EnumConst;
import com.chat.bee.utils.SocketUtils;
import com.chat.bee.utils.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Minh Tuấn
 * 6:46 CH 06/03/2021/03/2021
 */

@RestController
@RequestMapping("/api")
public class ChatAPI {

    @Autowired
    private IChatService chatService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUploadService uploadService;

    @Autowired
    private IMemberService memberService;

    @Autowired
    private SocketUtils socketUtils;

    @Autowired
    private ObjectMapper mapper;

//  --------------------------------------------------------------------------------------------------------------------

    @GetMapping("/friend-list")
    public List<UserDTO> friendList() {
        return userService.friendList();
    }

    @GetMapping("/request-contact-list")
    public List<UserDTO> requestContactList() {
        return userService.requestContactList();
    }

    @GetMapping("/get-group/{groupId}")
    public GroupChatDetailDTO getGroup(@PathVariable String groupId) {
        return chatService.getGroupChat(groupId);
    }

    @PostMapping("/group-list")
    public List<GroupChatDTO> groupList(@RequestParam(required = false) String lastedUpdate, @RequestParam int type) {
        Date date = null;
        if (lastedUpdate != null) {
            date = new Date(Long.parseLong(lastedUpdate));
        }
        return chatService.getGroupChats(date, type);
    }

    @GetMapping("/group-list-id")
    public List<String> groupListIds() {
        return chatService.getGroupIds();
    }

    @PostMapping("/upload")
    public List<String> uploadFile(List<MultipartFile> files) {
        return uploadService.uploadFile(files);
    }

    @PostMapping("/add-chat")
    public void addChat(@RequestBody ChatDTO chat) {
        chatService.addChat(chat);
    }

    @PostMapping("/edit-chat")
    public void editChat(@RequestParam String chatId, String content) {
        chatService.updateChat(chatId, content);
    }

    @GetMapping("/pin-chat/{chatId}")
    public void pinChat(@PathVariable String chatId) {
        chatService.pinChat(chatId);
    }

    @GetMapping("/delete-chat/{chatId}")
    public void deleteChat(@PathVariable String chatId) {
        chatService.deleteChat(chatId);
    }

    @GetMapping("/search-user/{query}")
    public List<UserDTO> searchUser(@PathVariable String query) {
        System.out.println(query);
        return userService.searchUser(query);
    }

    @PostMapping("/get-user-by-phone")
    public List<UserDTO> getUserByPhone(@RequestBody String phoneStr) throws JsonProcessingException {
        List<String> phones = mapper.readValue(phoneStr, List.class);
        return userService.getUserByPhone(phones);
    }

    @PostMapping("/create-group")
    public String createGroup(@RequestBody GroupChatDTO groupChat) {
        List<String> memberIds = groupChat.getMembers().stream().map(m -> m.getUser().getId()).collect(Collectors.toList());
        return chatService.createGroupChat(memberIds, groupChat);
    }

    @GetMapping("/request-contact/{friendId}")
    public void requestContact(@PathVariable String friendId) {
        userService.addRequest(friendId, null);
    }

    @GetMapping("/cancel-request-contact/{friendId}")
    public void cancelRequestContact(@PathVariable String friendId) {
        userService.removeRequest(friendId, null);
    }

    @GetMapping("/accept-request/{friendId}")
    public String acceptRequest(@PathVariable String friendId) {
        return userService.acceptRequest(friendId, null);
    }

    @GetMapping("/reject-request/{friendId}")
    public void rejectRequest(@PathVariable String friendId) {
        userService.rejectRequest(friendId, null);
    }

    @GetMapping("/delete-conversation/{groupId}")
    public void deleteConversation(@PathVariable String groupId) {
        memberService.deleteConversation(groupId);
    }

    @PostMapping("/change-group-name")
    public void changeGroupName(@RequestParam String groupId, @RequestParam String name) {
        chatService.changeGroupName(groupId, name);
    }

    @PostMapping("/add-member")
    public void addMember(@RequestParam String groupId, @RequestParam List<String> userIds) {
        chatService.addMember(groupId, userIds);
    }

    @GetMapping("/leave-group/{groupId}")
    public void leaveGroup(@PathVariable String groupId) {
        chatService.leaveGroup(groupId);
    }

    @PostMapping("/customize-screen")
    public void customizeScreen(@RequestParam String groupId, @RequestParam String chatStyle) throws JsonProcessingException {
        ChatStyleDTO chatStyleDTO = mapper.readValue(chatStyle, ChatStyleDTO.class);
        chatService.customizeScreen(groupId, chatStyleDTO);
    }

    @PostMapping("/change-nickname")
    public void changeNickname(@RequestParam String userId, @RequestParam String groupId, @RequestParam String nickname) {
        memberService.changeNickname(userId, groupId, nickname);
    }

    @GetMapping("/fake/user")
    public void fakeUser() {
        UserDTO user1 = UserDTO.builder().phone("358421348").countryCode("+84").firstName("Tạ").lastName("Vy")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user1, "123456");
        UserDTO user2 = UserDTO.builder().phone("358421366").countryCode("+84").firstName("Trần").lastName("Trinh")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user2, "123456");
        UserDTO user3 = UserDTO.builder().phone("358422345").countryCode("+84").firstName("Trịnh").lastName("Tâm")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user3, "123456");
        UserDTO user4 = UserDTO.builder().phone("358421848").countryCode("+84").firstName("Hà").lastName("Huê")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user4, "123456");
        UserDTO user5 = UserDTO.builder().phone("358427748").countryCode("+84").firstName("Nguyễn").lastName("Minh")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user5, "123456");
        UserDTO user6 = UserDTO.builder().phone("358421128").countryCode("+84").firstName("Trần").lastName("Nhã")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user6, "123456");
        UserDTO user7 = UserDTO.builder().phone("358429848").countryCode("+84").firstName("Lê").lastName("Vy")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user7, "123456");
        UserDTO user8 = UserDTO.builder().phone("358021348").countryCode("+84").firstName("Quách").lastName("Tuyên")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user8, "123456");
        UserDTO user9 = UserDTO.builder().phone("358421448").countryCode("+84").firstName("Tạ").lastName("Tốn")
                .gender(EnumConst.GenderEnum.FEMALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user9, "123456");
        UserDTO user10 = UserDTO.builder().phone("358421318").countryCode("+84").firstName("Lý").lastName("Hiện")
                .gender(EnumConst.GenderEnum.MALE.toString()).birthday(new Date()).build();
        userService.createChatUser(user10, "123456");
        UserDTO user11 = UserDTO.builder().phone("358421317").countryCode("+84").firstName("Trần").lastName("Minh Tuấn")
                .gender(EnumConst.GenderEnum.MALE.toString()).birthday(new Date())
                .avatarUrl("https://numpaint.com/wp-content/uploads/2020/08/japan-autumn-season-paint-by-number.jpg")
                .build();
        userService.createChatUser(user11, "123456");
    }

    @GetMapping("/fake/contact")
    public void fakeContact(@RequestParam String groupId) {
        ChatDTO chat = ChatDTO.builder()
                .content(EnumConst.NotificationTypeEnum.CONTACTED.toString())
                .groupId(groupId)
                .id(StringUtils.generateId(groupId))
                .chatTime(new Date())
                .type(EnumConst.ChatTypeEnum.NOTIFICATION.toString())
                .isSystem(true)
                .build();
        chatService.addChat(chat);
    }

    @GetMapping("/fake/test")
    public GroupChatDetailDTO fakeTest() {
        return chatService.getGroupChat("0358421347-1616343744180-1616343744247");
    }

    @GetMapping("/fake/friend")
    public void fakeFriend() {

    }
}
