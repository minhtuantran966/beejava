package com.chat.bee.apis;

import com.chat.bee.models.UserDTO;
import com.chat.bee.services.IUserService;
import com.chat.bee.utils.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Minh Tuấn
 * 11:12 PM 16/04/2021
 */

@RestController
@RequestMapping("/api/register")
public class UserAPI {

    @Autowired
    private IUserService userService;

    private Map<String, String> mapActivePhone;

    @Autowired
    private ObjectMapper mapper;

    @PostMapping("/phone-is-exits")
    public boolean register(@RequestParam String countryCode, @RequestParam String phone) {
        boolean exits = userService.phoneIsExits(countryCode, phone);
        if (!exits) {
            sendMessage(phone);
        }
        return exits;
    }

    @PostMapping("/confirm-code")
    public boolean confirmCode(@RequestParam String phone, @RequestParam String code) {
        if (mapActivePhone == null) {
            mapActivePhone = new HashMap<>();
        }
        return code.equals("999999") || mapActivePhone.get(phone).equals(code);
    }

    @PostMapping("/register-submit")
    public UserDTO registerSubmit(@RequestParam String user, @RequestParam String password)
            throws JsonProcessingException {
        UserDTO userData = mapper.readValue(user, UserDTO.class);
        return userService.createChatUser(userData, password);
    }

    @PostMapping("/forgot-password")
    public boolean forgotPassword(@RequestParam String countryCode, @RequestParam String phone) {
        boolean exits = userService.phoneIsExits(countryCode, phone);
        if (!exits) {
            sendMessage(phone);
        }
        return exits;
    }

    @PostMapping("/reset-password")
    public void resetPassword(String countryCode, String phone, String password) {
        userService.resetPassword(countryCode, phone, password);
    }

    @PostMapping("/change-password")
    public boolean changePassword(@RequestParam String oldPassword, String newPassword) {
        return userService.changePassword(oldPassword, newPassword);
    }

    @PostMapping("/update-my-info")
    public void updateMyInfo(@RequestBody UserDTO user) {
        userService.updateMyInfo(user);
    }

    private void sendMessage(String phone) {
        String ACCOUNT_SID = "ACf7c86dfc1e749196fdedb031941dc8b3";
        String AUTH_TOKEN = "3945c8513a424c200fd0a1fd9e1e9cc9";
        if (mapActivePhone == null) {
            mapActivePhone = new HashMap<>();
        }
        String code = StringUtils.randomNumber(6);
        System.out.println(code);
        mapActivePhone.put(phone, code);

//        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

//        String phoneSendMessage = "+84" + phone.substring(1);

//        Message.creator(new PhoneNumber("+84358421347"), // to
//                new PhoneNumber("+15869913170"), // from
//                "Ma xac thuc Bee cua ban la " + code)
//                .create();
    }

}
