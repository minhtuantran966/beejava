package com.chat.bee.repositories;

import com.chat.bee.entities.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserRepository extends PagingAndSortingRepository<User, String> {

    List<User> findAllByIdIn(List<String> userIds);

    @Query("{ $or: [ {phone : ?0}, {firstName : {$regex : /?0/, $options: 'im'}}, {lastName : {$regex : /?0/, $options: 'im'}}, {fullNameMod : {$regex : /?0/, $options: 'im'}}]}")
    List<User> findAllByPhoneLikeOrFirstNameLikeOrLastNameLike(String query);

    List<User> findAllByPhoneIn(List<String> phones);

}
