package com.chat.bee.repositories;

import com.chat.bee.entities.GroupChat;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IGroupChatRepository extends PagingAndSortingRepository<GroupChat, String> {

    GroupChat findGroupChatById(String id);

    List<GroupChat> findAllByIdInAndTypeInAndLastedUpdateIsAfterOrderByLastedUpdateDesc(List<String> groupIds, List<String> types, Date lastedUpdate, Pageable pageable);

    List<GroupChat> findAllByIdInAndTypeInOrderByLastedUpdateDesc(List<String> groupIds, List<String> types, Pageable pageable);

}
