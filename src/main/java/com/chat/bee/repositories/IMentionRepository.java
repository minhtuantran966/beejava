package com.chat.bee.repositories;

import com.chat.bee.entities.Mention;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMentionRepository extends PagingAndSortingRepository<Mention, String> {

    List<Mention> findAllByChatIdIn(List<String> chatIds);

}
