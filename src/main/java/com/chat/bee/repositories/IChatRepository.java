package com.chat.bee.repositories;

import com.chat.bee.entities.Chat;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IChatRepository extends PagingAndSortingRepository<Chat, String> {

    List<Chat> findChatByGroupId(String groupId, Pageable pageable);

    List<Chat> findAllByIdIn(List<String> chatIds);

}
