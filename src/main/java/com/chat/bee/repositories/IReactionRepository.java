package com.chat.bee.repositories;

import com.chat.bee.entities.Reaction;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IReactionRepository extends PagingAndSortingRepository<Reaction, String> {

    List<Reaction> findAllByChatIdIn(List<String> chatIds);

}
