package com.chat.bee.repositories;

import com.chat.bee.entities.GroupMember;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IGroupMemberRepository extends PagingAndSortingRepository<GroupMember, String> {

    List<GroupMember> findAllByGroupIdIn(List<String> groupIds);

    List<GroupMember> findAllByUserIdIn(List<String> userIds);

    List<GroupMember> findAllByUserId(String userId);

    void deleteByGroupIdAndUserId(String groupId, String userId);

    GroupMember findAllByGroupIdAndUserId(String groupId, String userId);

}
