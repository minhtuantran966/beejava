package com.chat.bee.repositories;

import com.chat.bee.entities.ConversationMember;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IConversationMemberRepository extends PagingAndSortingRepository<ConversationMember, String> {

    List<ConversationMember> findAllByGroupId(String groupId);

    List<ConversationMember> findAllByUserId(String userId);

    void deleteByGroupIdAndUserId(String groupId, String userId);

}
