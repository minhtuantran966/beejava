package com.chat.bee.repositories;

import com.chat.bee.entities.ChatNotification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IChatNotificationRepository extends PagingAndSortingRepository<ChatNotification, String> {
}
