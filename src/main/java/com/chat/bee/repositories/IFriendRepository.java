package com.chat.bee.repositories;

import com.chat.bee.entities.Friend;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFriendRepository extends CrudRepository<Friend, String> {

    @Query("{$and : [{fromId : { $in : ?0 }},{toId : { $in : ?0 }}]}")
    List<Friend> findAllByFromIdInAndToIdIn(List<String> userIds);

    @Query("{$or : [{fromId : { $in : ?0 }},{toId : { $in : ?0 }}]}")
    List<Friend> findAllByFromIdInAOrToIdIn(List<String> userIds);

}
