package com.chat.bee.repositories;

import com.chat.bee.entities.InviteRequest;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IInviteRequestRepository extends PagingAndSortingRepository<InviteRequest, String> {

    void removeByFromIdAndToIdAndGroupId(String fromId, String toId, String groupId);

    List<InviteRequest> findAllByFromIdInAndToIdInAndGroupId(List<String> fromIds, List<String> toIds, String groupId);

    List<InviteRequest> findAllByToIdAndGroupId(String myId, String groupId);

}
