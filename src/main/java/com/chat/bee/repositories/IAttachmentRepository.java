package com.chat.bee.repositories;

import com.chat.bee.entities.Attachment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAttachmentRepository extends PagingAndSortingRepository<Attachment, String> {

    List<Attachment> findAllByChatIdIn(List<String> chatIds);

}
