package com.chat.bee.repositories;

import com.chat.bee.entities.ChatStyle;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IChatStyleRepository extends CrudRepository<ChatStyle, String> {

    List<ChatStyle> findAllByGroupIdIn(List<String> groupIds);

}
