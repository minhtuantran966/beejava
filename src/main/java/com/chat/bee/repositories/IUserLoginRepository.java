package com.chat.bee.repositories;

import com.chat.bee.entities.UserLogin;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserLoginRepository extends PagingAndSortingRepository<UserLogin, String> {

    UserLogin findUserLoginByCountryCodeAndPhone(String countryCode, String phone);

    @Query("{ $or: [ {phone : ?0}, {username : ?0}]}")
    UserLogin findUserLoginByPhoneOrUsernameIs(String username);

    UserLogin findByUserId(String userId);

}
