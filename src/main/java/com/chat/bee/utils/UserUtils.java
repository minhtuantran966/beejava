package com.chat.bee.utils;

import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Minh Tuấn
 * 8:16 CH 14/03/2021/03/2021
 */

public class UserUtils {

    private static String currentUserId = null;

    public static String getCurrentUserId() {
        if (currentUserId == null){
            currentUserId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        }
        return currentUserId;
    }

    public static void setCurrentUserId(String userId) {
        currentUserId = userId;
    }

}
