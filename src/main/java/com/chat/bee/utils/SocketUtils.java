package com.chat.bee.utils;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;

/**
 * Minh Tuấn
 * 2:35 CH 17/02/2021/02/2021
 */

@Component
public class SocketUtils {

    private Socket socket;
    @Value("${socket.url}")
    private String url;

    public void connect() {
        try {
            socket = IO.socket(url);
            if (!socket.connected()) {
                socket.connect();
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        socket.disconnect();
    }

    public Socket getSocket() {
        if (socket == null) {
            connect();
        }
        return socket;
    }

}
