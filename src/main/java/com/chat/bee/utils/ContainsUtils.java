package com.chat.bee.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Minh Tuấn
 * 10:28 CH 13/03/2021/03/2021
 */

public class ContainsUtils {

    public static int CHAT_PER_PAGE = 50;
    public static int GROUP_CHAT_PER_PAGE = 20;
    public static String DATE_FORMAT_TYPE = "yyyy-MM-dd'T'HH:mm:ssZ";

    public static List<String> systemChatType() {
        List<String> systemChatType = new ArrayList<>();
        systemChatType.add(EnumConst.ChatTypeEnum.NOTIFICATION.toString());
        return systemChatType;
    }

}
