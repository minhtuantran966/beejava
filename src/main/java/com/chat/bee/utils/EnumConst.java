package com.chat.bee.utils;

public class EnumConst {

    public enum SortFieldEnum {
        time
    }

    public enum ChatTypeEnum {
        CHAT,
        NOTIFICATION,
    }

    public enum AttachmentTypeEnum {
        IMAGE,
        VIDEO,
        FILE,
        LINK,
        AUDIO
    }

    public enum NotificationTypeEnum {
        CONTACTED,
        PIN_CHAT,
        UP_PIN_CHAT,
    }

    public enum GenderEnum {
        MALE,
        FEMALE,
        OTHER
    }

    public enum UserRoleEnum {
        USER,
        ADMIN
    }

    public enum GroupChatTypeEnum {
        PRIVATE,
        PUBLIC,
        FRIEND,
    }

    public enum SocketListener {
        JOIN_GROUP,
        NEW_CHAT,
        REMOVE_CHAT,
        PIN_CHAT,
    }

}
