package com.chat.bee.configs;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Minh Tuấn
 * 2:09 SA 09/04/2021
 */

@Configuration
public class S3Config {

    @Value("${s3.accesskey}")
    private String accessKey;
    @Value("${s3.secretkey}")
    private String secretKey;
    @Value("${s3.endpoint}")
    private String endpoint;

    @Bean
    public AmazonS3 getAmazonS3Cient() {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(Protocol.HTTP);

        //noinspection deprecation
        AmazonS3 s3 = new AmazonS3Client(credentials, clientConfig);
        s3.setEndpoint(endpoint);
        return s3;
    }

}
