package com.chat.bee.configs;

import com.chat.bee.utils.ContainsUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;

/**
 * Minh Tuấn
 * 22:07 09/05/2021
 */

@Configuration
public class BeeConfig {

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat(ContainsUtils.DATE_FORMAT_TYPE));
        return mapper;
    }
}